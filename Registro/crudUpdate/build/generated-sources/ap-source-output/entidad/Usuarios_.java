package entidad;

import entidad.Documentos;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2016-06-04T13:13:13")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> nombre;
    public static volatile CollectionAttribute<Usuarios, Documentos> documentosCollection3;
    public static volatile CollectionAttribute<Usuarios, Documentos> documentosCollection1;
    public static volatile SingularAttribute<Usuarios, Integer> idUsuario;
    public static volatile CollectionAttribute<Usuarios, Documentos> documentosCollection2;
    public static volatile SingularAttribute<Usuarios, String> apellidos;
    public static volatile SingularAttribute<Usuarios, Integer> tipo;
    public static volatile CollectionAttribute<Usuarios, Documentos> documentosCollection;
    public static volatile SingularAttribute<Usuarios, String> password;
    public static volatile SingularAttribute<Usuarios, String> correo;

}