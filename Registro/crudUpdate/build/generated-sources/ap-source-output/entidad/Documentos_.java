package entidad;

import entidad.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2016-06-04T13:13:13")
@StaticMetamodel(Documentos.class)
public class Documentos_ { 

    public static volatile SingularAttribute<Documentos, String> nombre;
    public static volatile SingularAttribute<Documentos, Usuarios> idUsuario;
    public static volatile SingularAttribute<Documentos, Integer> status;
    public static volatile SingularAttribute<Documentos, byte[]> documento;
    public static volatile SingularAttribute<Documentos, Date> fechaCreacion;
    public static volatile SingularAttribute<Documentos, Date> fechaAutorizacion;
    public static volatile SingularAttribute<Documentos, Integer> idDocumento;
    public static volatile SingularAttribute<Documentos, Usuarios> idRev1;
    public static volatile SingularAttribute<Documentos, Usuarios> idAutorizador;
    public static volatile SingularAttribute<Documentos, Usuarios> idRev2;

}