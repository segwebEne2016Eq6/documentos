/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sesion;

import entidad.Documentos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author TOSHIBA
 */
@Stateless
public class DocumentosFacade extends AbstractFacade<Documentos> {
    @PersistenceContext(unitName = "crudUpdatePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocumentosFacade() {
        super(Documentos.class);
    }
    
    public List<Documentos> docsRev1() {
        String sql = "SELECT d FROM Documentos d WHERE d.status = :status";
        Query consulta = getEntityManager().createQuery(sql);
        consulta.setParameter("status", 0);
        
        return consulta.getResultList();
    } 
    
}
