/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author IthzaVG
 */
@Entity
@Table(name = "revisiones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revisiones.findAll", query = "SELECT r FROM Revisiones r"),
    @NamedQuery(name = "Revisiones.findByIdRevision", query = "SELECT r FROM Revisiones r WHERE r.idRevision = :idRevision"),
    @NamedQuery(name = "Revisiones.findByFecha", query = "SELECT r FROM Revisiones r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "Revisiones.findByPagina", query = "SELECT r FROM Revisiones r WHERE r.pagina = :pagina"),
    @NamedQuery(name = "Revisiones.findByParrafo", query = "SELECT r FROM Revisiones r WHERE r.parrafo = :parrafo"),
    @NamedQuery(name = "Revisiones.findByComentarios", query = "SELECT r FROM Revisiones r WHERE r.comentarios = :comentarios"),
    @NamedQuery(name = "Revisiones.findByStatus", query = "SELECT r FROM Revisiones r WHERE r.status = :status")})
public class Revisiones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_revision")
    private Integer idRevision;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pagina")
    private int pagina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "parrafo")
    private int parrafo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "comentarios")
    private String comentarios;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @JoinColumn(name = "id_documento", referencedColumnName = "id_documento")
    @ManyToOne(optional = false)
    private Documentos idDocumento;
    @JoinColumn(name = "id_revisor", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idRevisor;

    public Revisiones() {
    }

    public Revisiones(Integer idRevision) {
        this.idRevision = idRevision;
    }

    public Revisiones(Integer idRevision, int pagina, int parrafo, String comentarios, int status) {
        this.idRevision = idRevision;
        this.pagina = pagina;
        this.parrafo = parrafo;
        this.comentarios = comentarios;
        this.status = status;
    }

    public Integer getIdRevision() {
        return idRevision;
    }

    public void setIdRevision(Integer idRevision) {
        this.idRevision = idRevision;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public int getParrafo() {
        return parrafo;
    }

    public void setParrafo(int parrafo) {
        this.parrafo = parrafo;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Documentos getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Documentos idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Usuarios getIdRevisor() {
        return idRevisor;
    }

    public void setIdRevisor(Usuarios idRevisor) {
        this.idRevisor = idRevisor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRevision != null ? idRevision.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revisiones)) {
            return false;
        }
        Revisiones other = (Revisiones) object;
        if ((this.idRevision == null && other.idRevision != null) || (this.idRevision != null && !this.idRevision.equals(other.idRevision))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Revisiones[ idRevision=" + idRevision + " ]";
    }
    
}
