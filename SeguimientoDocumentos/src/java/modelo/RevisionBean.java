/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import control.Revision;
import entity.Documentos;
import entity.Revisiones;
import entity.Usuarios;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import sesion.DocumentoFacade;
import sesion.RevisionFacade;
import sesion.UsuarioFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class RevisionBean {

    private Date fecha;
    private int pagina;
    private int parrafo;
    private int status;
    private String comentario;
    private int idRevisor;
    private int idDoc;
    private int idRevision;

    @Inject
    private RevisionFacade revFacade;
    private Revisiones selected = new Revisiones();

    @EJB
    private RevisionFacade ejbFacade;
    private List<Documentos> documentos = null;
    private List<Usuarios> revisor = null;

    public RevisionFacade getRevFacade() {
        return revFacade;
    }

    public void setRevFacade(RevisionFacade revFacade) {
        this.revFacade = revFacade;
    }

    public Revisiones getSelected() {
        return selected;
    }

    public void setSelected(Revisiones selected) {
        this.selected = selected;
    }

    public RevisionFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(RevisionFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public List<Documentos> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documentos> documentos) {
        this.documentos = documentos;
    }

    public List<Usuarios> getRevisor() {
        return revisor;
    }

    public void setRevisor(List<Usuarios> revisor) {
        this.revisor = revisor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public int getParrafo() {
        return parrafo;
    }

    public void setParrafo(int parrafo) {
        this.parrafo = parrafo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getIdRevisor() {
        return idRevisor;
    }

    public void setIdRevisor(int idRevisor) {
        this.idRevisor = idRevisor;
    }

    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdRevision() {
        return idRevision;
    }

    public void setIdRevision(int idRevision) {
        this.idRevision = idRevision;
    }

    public List<Revisiones> getRevisiones() {
        return this.revFacade.findAll();
    }
    
     private DocumentoFacade getFacade() {
        return ejbFacadeD;
    }
    @EJB
    private sesion.DocumentoFacade ejbFacadeD;
    public List<Documentos> getItemsAvailableSelectOneD() {
        return getFacade().findAll();
    }
   
    
    private UsuarioFacade getFacadeU() {
        return ejbFacadeU;
    }
    @EJB
    private sesion.UsuarioFacade ejbFacadeU;
    public List<Usuarios> getItemsAvailableSelectOneR() {
        return getFacadeU().findAll();
    }
    

    public String revDoc(int id) {

        Revisiones r = this.revFacade.find(id);
        this.pagina = r.getPagina();
        this.parrafo = r.getParrafo();
        this.comentario = r.getComentarios();
        this.idRevisor = r.getIdRevisor().getIdUsuario();
        this.idDoc = r.getIdDocumento().getIdDocumento();
        this.fecha = r.getFecha();

        return "dialogViews/observacionesDF";
    }

    public String eliminaObs(int id) {
        Revisiones rev = this.revFacade.find(id);
        this.revFacade.remove(rev);
        return "observacionesDF";

    }

    public String getIds(int id) {
        Revisiones ids = this.ejbFacade.find(id);
        this.idRevisor = ids.getIdRevisor().getIdUsuario();
        this.idDoc = ids.getIdDocumento().getIdDocumento();

        return "agregaObsDF";
    }

    public String agregaObs() {
        Revisiones r = new Revisiones();
        r.setIdDocumento(selected.getIdDocumento());
        r.setIdRevisor(selected.getIdRevisor());
        r.setPagina(selected.getPagina());
        r.setParrafo(selected.getParrafo());
        r.setComentarios(selected.getComentarios());
        r.setFecha(selected.getFecha());

        this.revFacade.create(r);
        this.idRevision = r.getIdRevision();
        return "agregaObsDF";

    }

}
