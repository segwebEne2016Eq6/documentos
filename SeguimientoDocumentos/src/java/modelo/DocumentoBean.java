/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import control.DocRev1Control;
import control.DocumentoControl;
import control.Documento;
import control.Revision;
import control.Revisores;
import control.RevisoresControl;
import entity.Revisiones;
import entity.Usuarios;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;
import sesion.RevisionFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocumentoBean {
    
    private UploadedFile file;
    private Collection<Documento> listaDocumentos = null;
    private Collection<Documento> listaDocRev1= null;
    private String nombre;
    private  int status, idDoc;
    private Usuarios idUsuario, idRev1,idRev2,idAutorizador;
    private final java.util.Date fecha = new Date(); 
    
    
    @Inject
    DocumentoControl creaDocumento;
    DocRev1Control revDoc;
    Documento documento;
    

    /**
     * Creates a new instance of fileBean
     */
    public DocumentoBean() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuarios getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(Usuarios idRev1) {
        this.idRev1 = idRev1;
    }

    public Usuarios getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(Usuarios idRev2) {
        this.idRev2 = idRev2;
    }

    public Usuarios getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(Usuarios idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

    

    
    public void upload() throws IOException{
      InputStream input = file.getInputstream();
      byte[] doc = IOUtils.toByteArray(input);
        
        documento = new Documento(idDoc,doc,nombre,status,idUsuario,idRev1,idRev2,idAutorizador,fecha);
        
        if (creaDocumento.agregaDocumento(documento)) {
            
            FacesMessage msg = new FacesMessage("Se guardó correctamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        }else{
            FacesMessage msg = new FacesMessage("Ocurrió un error al guardar su archivo intente nuevamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }

    
    
     public Collection<Documento> getDocumentos(){
        
        listaDocumentos= creaDocumento.listaDocus();
        return listaDocumentos;
        
    }

  

    
}
