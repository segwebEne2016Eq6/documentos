/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author lili
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String correo;
    private String password;

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String login() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pagina = null;
        try {
            System.out.println("datos"+correo+password);
            request.login(correo, password);
            if (request.isUserInRole("1")) {
                pagina = "/1/creador";
            } else {
                if (request.isUserInRole("2")) {
                    pagina = "/2/revisor1";
                } else 
                
                if (request.isUserInRole("3")) {
                    pagina = "/3/revisor2";
                } else
                 
                 if (request.isUserInRole("4")) {
                    pagina = "/4/autorizador";
                } else
                 
                {
                 
                    context.addMessage(null, new FacesMessage("Credenciales insuficientes"));
                    pagina = "/faces/login_error.xhtml";
                }
            }
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage("Error en la convinación usuario/contraseña"));
            System.out.println("error-----------------------------------------------------");
        }
        return pagina;
    }

    public void logout() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        externalContext.redirect(externalContext.getRequestContextPath() + "/faces/login.xhtml");
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

 

}
