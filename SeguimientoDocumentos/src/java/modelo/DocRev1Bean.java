/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import control.DocRev1Control;
import control.DocumentoControl;
import control.Documento;
import control.DocumentoRevisor;
import control.Revisores;
import control.RevisoresControl;
import entity.Usuarios;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocRev1Bean {
    
    private UploadedFile file;
    private int idDoc;
    private Collection<DocumentoRevisor> listaDocRev1= null;
    private String nombre;
    private  int status;
    private Usuarios idUsuario, idRev1,idRev2,idAutorizador;
    private final java.util.Date fecha = new Date();
    
    
    @Inject
    DocRev1Control revDoc;
    Documento documento;
    

    /**
     * Creates a new instance of fileBean
     */
    public DocRev1Bean() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuarios getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(Usuarios idRev1) {
        this.idRev1 = idRev1;
    }

    public Usuarios getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(Usuarios idRev2) {
        this.idRev2 = idRev2;
    }

    public Usuarios getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(Usuarios idAutorizador) {
        this.idAutorizador = idAutorizador;
    }


    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }


    
     
     public Collection<DocumentoRevisor> getDocumentosRev1(){
         listaDocRev1 = revDoc.listaRD();
         return listaDocRev1;
     }
   
    
}
