/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;


import entity.Documentos;
import entity.Usuarios;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author IthzaVG
 */
@Stateless
public class DocumentoFacade extends AbstractFacade<Documentos> {
    
      @PersistenceContext(unitName = "SeguimientoDocumentosPU")
    private EntityManager em;

    public DocumentoFacade() {
        super(Documentos.class);
    }

     @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Usuarios findByIdRevisor1(int id){
        Usuarios revisores = null;
        List <Usuarios> revisor;
        Query consulta = em.createNamedQuery("Usuarios.findByIdUsuario");
        consulta.setParameter("id", id);
        revisor = consulta.getResultList();
        if (!revisor.isEmpty()) 
            revisores = revisor.get(0);
          return revisores;
    }
    
    public List<Documentos> findAllbyOrder(){
        
        String sql = "SELECT d FROM Documentos d";
        Query consulta = em.createQuery(sql);
        return consulta.getResultList();
        
        
    }
    
    public List<Usuarios> findRevisorUno(int tipo){
        String sql = "SELECT u FROM Usuarios u WHERE u.tipo = :tipo";
         Query consulta = em.createQuery(sql);
         consulta.setParameter("tipo", tipo);
        //consulta.setParameter("tipo", tipo);
      
        return consulta.getResultList();
    }
    
  
        public List<Usuarios> findRevisorDos(int tipo){
        String sql = "SELECT u FROM Usuarios u WHERE u.tipo = :tipo";
         Query consulta = em.createQuery(sql);
         consulta.setParameter("tipo", tipo);
        //consulta.setParameter("tipo", tipo);
      
        return consulta.getResultList();
    }
    
    
    
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void save(Documentos documento) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Documentos>> constraintViolations = validator.validate(documento);
        if (constraintViolations.size() > 0) {
            Iterator<ConstraintViolation<Documentos>> iterator = constraintViolations.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<Documentos> cv = iterator.next();
                System.out.println("mensaje de error de prevalidacion " + cv.getMessage());
                System.out.println("en el atriburo " + cv.getPropertyPath());
            }
        }
        em.persist(documento);
        em.flush();
        em.refresh(documento);
    }

  
    
    
}
