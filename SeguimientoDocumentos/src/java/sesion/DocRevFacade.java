/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;


import entity.Documentos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


/**
 *
 * @author IthzaVG
 */
@Stateless
public class DocRevFacade extends AbstractFacade<Documentos> {
    
      @PersistenceContext(unitName = "SeguimientoDocumentosPU")
    private EntityManager em;

    public DocRevFacade() {
        super(Documentos.class);
    }

     @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    

    
  
        public List<Documentos> findDocRevisorDos(int status){
        String sql = "SELECT d FROM Documentos d WHERE d.status = :status";
         Query consulta = em.createQuery(sql);
         //consulta.setParameter("id_rev1", id);
         consulta.setParameter("status", status);
        //consulta.setParameter("tipo", tipo);
      
        return consulta.getResultList();
        }
    
}
