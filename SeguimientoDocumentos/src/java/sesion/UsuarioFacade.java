/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;


import entity.Documentos;
import entity.Revisiones;
import entity.Usuarios;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


/**
 *
 * @author IthzaVG
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "SeguimientoDocumentosPU")
    private EntityManager em;

    public UsuarioFacade() {
        super(Usuarios.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
 
