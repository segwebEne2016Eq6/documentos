/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;


import entity.Usuarios;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocumentoRevisor {

   private byte[] file;
   private String nombre;
   private int status;
   private Usuarios idUsuario;
   private Usuarios idRev1;
   private Usuarios idRev2;
   private Usuarios idAutorizador;
   private Date fecha = new Date();
   private int idDocumento;

    public DocumentoRevisor(int idDocumento,byte[] file, String nombre, int status, Usuarios idUsuario, Usuarios idRev1, Usuarios idRev2, Usuarios idAutorizador, Date fecha) {
       this.idDocumento = idDocumento;
        this.file = file;
        this.nombre = nombre;
        this.status = status;
        this.idUsuario = idUsuario;
        this.idRev1 = idRev1;
        this.idRev2 = idRev2;
        this.idAutorizador = idAutorizador;
        this.fecha = fecha;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuarios getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(Usuarios idRev1) {
        this.idRev1 = idRev1;
    }

    public Usuarios getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(Usuarios idRev2) {
        this.idRev2 = idRev2;
    }

    public Usuarios getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(Usuarios idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }


   
    
}
