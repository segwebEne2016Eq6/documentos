/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import entity.Documentos;
import entity.Usuarios;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import sesion.DocRevFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocRev1Control {
    
    private List<Documentos> listaDocRev1;
    private Usuarios idRev1;

    /**
     * Creates a new instance of DocRev1Control
     */
    public DocRev1Control() {
    }
    
    @Inject
    private DocRevFacade docFac;
    
    @PostConstruct
    public void listaDocsRev1(){
        listaDocRev1 = docFac.findDocRevisorDos(0);
    }
    
    public Collection<DocumentoRevisor> listaRD(){
        Collection<DocumentoRevisor> documentos;
        documentos = armaListaRD();
        return documentos;
    }
    public Collection <DocumentoRevisor> armaListaRD(){
          Usuarios idUsuario, idRevi1, idRev2, idAutorizador;
        int status, idDoc;
        String nombre;
        byte[] file;
        Date fecha;

        Collection<DocumentoRevisor> listFile = new ArrayList();
        int i = 0;
        for (Documentos doc : listaDocRev1) {
            i++;
            idUsuario = doc.getIdUsuario();
            idRev2 = doc.getIdRev2();
            idRevi1 = doc.getIdRev1();
            idAutorizador = doc.getIdAutorizador();
            status = doc.getStatus();
            nombre = doc.getNombre();
            file = doc.getDocumento();
            fecha = doc.getFechaCreacion();
            idDoc = doc.getIdDocumento();

            DocumentoRevisor d = new DocumentoRevisor(idDoc,file, nombre, status, idUsuario, idRevi1, idRev2, idAutorizador, fecha);
            listFile.add(d);
        }

        return listFile;
    }
    
    
}
