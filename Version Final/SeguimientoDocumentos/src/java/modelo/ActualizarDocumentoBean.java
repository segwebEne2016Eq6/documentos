/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import entity.Documentos;
import entity.Revisiones;
import entity.Usuarios;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import sesion.DocumentoFacade;
import sesion.RevisionFacade;
import sesion.UsuarioFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean(name = "updateDoc")
@RequestScoped
public class ActualizarDocumentoBean {

    private UploadedFile file;
    private byte[] arraydoc;
    private String nombre;
    private int status, idDoc;
    private int idUsuario, idRev1, idRev2, idAutorizador;
    private Date fecha = new Date();

    @Inject
    private DocumentoFacade docFacade;
    private Documentos selected = new Documentos();

    @EJB
    private sesion.RevisionFacade ejbFacade;
    private sesion.UsuarioFacade ejbFac;
    private List<Revisiones> revisiones = null;
    private List<Usuarios> usuarios = null;

    public UsuarioFacade getEjbFac() {
        return ejbFac;
    }

    public List<Usuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(int idRev1) {
        this.idRev1 = idRev1;
    }

    public int getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(int idRev2) {
        this.idRev2 = idRev2;
    }

    public int getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(int idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public DocumentoFacade getDocFacade() {
        return docFacade;
    }

    public void setDocFacade(DocumentoFacade docFacade) {
        this.docFacade = docFacade;
    }

    public byte[] getArraydoc() {
        return arraydoc;
    }

    public void setArraydoc(byte[] arraydoc) {
        this.arraydoc = arraydoc;
    }

    public Documentos getSelected() {
        return selected;
    }

    public void setSelected(Documentos selected) {
        this.selected = selected;
    }

    public RevisionFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(RevisionFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<Revisiones> getRevisiones() {
        return revisiones;
    }

    public void setRevisiones(List<Revisiones> revisiones) {
        this.revisiones = revisiones;
    }

    public List<Revisiones> getItemsAvailableSelectOne() {
        return getEjbFacade().findAll();
    }

    public List<Documentos> getDocumentos() {

        return this.docFacade.findAll();
    }

    public String update(int id) throws IOException {

        Documentos d = this.docFacade.find(id);
        System.out.println(d);
        this.idDoc = d.getIdDocumento();
        this.idUsuario = d.getIdUsuario().getIdUsuario();
        this.idRev1 = d.getIdRev1().getIdUsuario();
        this.idRev2 = d.getIdRev2().getIdUsuario();
        this.idAutorizador = d.getIdAutorizador().getIdUsuario();
        this.arraydoc = d.getDocumento();
        this.nombre = d.getNombre();
        this.fecha = d.getFechaCreacion();
        this.status = d.getStatus();
        System.out.println(d.getIdAutorizador().getIdUsuario());
        return "/dialogViews/actualizarDF";

    }
    
    public String updateS(int id) throws IOException {

        Documentos d = this.docFacade.find(id);
        System.out.println(d);
        this.idDoc = d.getIdDocumento();
        this.idUsuario = d.getIdUsuario().getIdUsuario();
        this.idRev1 = d.getIdRev1().getIdUsuario();
        this.idRev2 = d.getIdRev2().getIdUsuario();
        this.idAutorizador = d.getIdAutorizador().getIdUsuario();
        this.arraydoc = d.getDocumento();
        this.nombre = d.getNombre();
        this.fecha = d.getFechaCreacion();
        this.status = d.getStatus();
        System.out.println(d.getIdAutorizador().getIdUsuario());
        return "/dialogViews/actualizarStatus";

    }

    public String saveUpdate(ActualizarDocumentoBean up, int id) throws IOException {
        InputStream input = file.getInputstream();
        byte[] guardadoc = IOUtils.toByteArray(input);

        Documentos doc = new Documentos();
        Usuarios creador = new Usuarios(1);
        Usuarios revuno = new Usuarios(2);
        Usuarios revdos = new Usuarios(3);
        Usuarios autorizador = new Usuarios(4);

        doc.setIdDocumento(id);
        doc.setNombre(up.getNombre());
        doc.setDocumento(guardadoc);
        doc.setFechaCreacion(up.getFecha());
        doc.setStatus(up.getStatus());
        doc.setIdUsuario(creador);
        doc.setIdRev1(revuno);
        doc.setIdRev2(revdos);
        doc.setIdAutorizador(autorizador);

        this.docFacade.edit(doc);

        FacesMessage msg = new FacesMessage("Se actualizó correctamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "/dialogViews/actualizarDF";
    }
    
     public String saveUpdateS(ActualizarDocumentoBean up, int id) throws IOException {
//        InputStream input = file.getInputstream();
//        byte[] guardadoc = IOUtils.toByteArray(input);
         
        Documentos doc = new Documentos();
        Usuarios creador = new Usuarios(1);
        Usuarios revuno = new Usuarios(2);
        Usuarios revdos = new Usuarios(3);
        Usuarios autorizador = new Usuarios(4);

        doc.setIdDocumento(id);
        doc.setNombre(up.getNombre());
        doc.setDocumento(up.getArraydoc());
        doc.setFechaCreacion(up.getFecha());
        doc.setStatus(up.getStatus());
        doc.setIdUsuario(creador);
        doc.setIdRev1(revuno);
        doc.setIdRev2(revdos);
        doc.setIdAutorizador(autorizador);

        this.docFacade.edit(doc);

        FacesMessage msg = new FacesMessage("Se actualizó correctamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "/dialogViews/actualizarStatus";
    }

    public String eliminaDoc(int id) {
        Documentos doc = this.docFacade.find(id);
        this.docFacade.remove(doc);
        return "creador";
    }
}
