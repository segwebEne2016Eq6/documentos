/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import control.DocRev1Control;
import control.Documento;
import control.DocumentoRevisor;
import entity.Usuarios;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class VerDocRev1Bean {
    

    private StreamedContent verDoc;
    private int idDoc;
    private String nombre;
    private  int status;
    private Usuarios idUsuario, idRev1,idRev2,idAutorizador;
    private final java.util.Date fecha = new Date();
    
    

    /**
     * Creates a new instance of fileBean
     */
    public VerDocRev1Bean() {
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuarios getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(Usuarios idRev1) {
        this.idRev1 = idRev1;
    }

    public Usuarios getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(Usuarios idRev2) {
        this.idRev2 = idRev2;
    }

    public Usuarios getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(Usuarios idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public StreamedContent getVerDoc() {
        return verDoc;
    }

    public void setVerDoc(StreamedContent verDoc) {
        this.verDoc = verDoc;
    }

    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

     
     
      public void ver(ActionEvent event) throws IOException, ClassNotFoundException{
       final String id_documento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_documento");
          System.out.println(id_documento +"holaholaid");
          int nuevoId = Integer.parseInt(id_documento);
          System.out.println(nuevoId +"nuevonuevoid");
          //System.out.println(idDoc);
        ResultSet st;
        try {
            byte[] bytes = null;
            
            Class.forName("com.mysql.jdbc.Driver");
             Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sdd_db?user=jsf&password=123456");
             
             PreparedStatement pt = con.prepareStatement("SELECT documento FROM Documentos WHERE id_documento = (?)");
             
             pt.setInt(1, nuevoId);
             System.out.println(pt);
             
             st = pt.executeQuery();
             
             while(st.next()){
                 bytes = st.getBytes("documento");
                 
             }
             
             con.close();
             
             FacesMessage message = new FacesMessage("Vista exitosa");
             FacesContext.getCurrentInstance().addMessage(null, message);
             
             HttpServletResponse response  = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
             response.getOutputStream().write(bytes);
             response.getOutputStream().close();
             
             FacesContext.getCurrentInstance().responseComplete();
        } catch (SQLException e) 
        {
            FacesMessage message = new FacesMessage("Error de conexión"  + "\n"+ e.getSQLState() + "\n" + e.getErrorCode());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
     
    
}
