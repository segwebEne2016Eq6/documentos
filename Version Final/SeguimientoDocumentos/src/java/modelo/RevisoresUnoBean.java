/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import control.Documento;
import control.Revisores;
import control.RevisoresControl;
import java.util.Collection;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class RevisoresUnoBean {
private int id;
private String nombre;
private String apellidos;
private String correo;
private String password;
private int tipo;

private Collection<Revisores> listaRevisoresUno = null;

@Inject
 RevisoresControl muestraRevisorUno;

    public RevisoresUnoBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
 
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
   
    
    public Collection<Revisores> getRevisores(){
       
             listaRevisoresUno = muestraRevisorUno.listaRevisores();
        
        return listaRevisoresUno;
    }
}
