/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import control.DocAutControl;
import control.DocRev1Control;
import control.DocumentoControl;
import control.Documento;
import control.DocumentoRevisor;
import control.Revisores;
import control.RevisoresControl;
import entity.Documentos;
import entity.Usuarios;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import sesion.DocumentoFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocAutBean {

    private UploadedFile file;
    private int idDoc;
    private byte[] arraydoc;
    private Collection<DocumentoRevisor> listaAut = null;
    private String nombre;
    private int status;
    private int idUsuario, idRev1, idRev2, idAutorizador;
    private Date fecha = new Date();

    @Inject
    DocAutControl autDoc;
    Documento documento;
    DocumentoFacade docFacade;

    @EJB
    private sesion.UsuarioFacade ejbFac;

    /**
     * Creates a new instance of fileBean
     */
    public DocAutBean() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(int idRev1) {
        this.idRev1 = idRev1;
    }

    public int getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(int idRev2) {
        this.idRev2 = idRev2;
    }

    public int getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(int idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public int getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

    public byte[] getArraydoc() {
        return arraydoc;
    }

    public void setArraydoc(byte[] arraydoc) {
        this.arraydoc = arraydoc;
    }

    public Collection<DocumentoRevisor> getDocumentosAut() {
        listaAut = autDoc.armaListaAut();
        return listaAut;
    }

   
}
