/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import modelo.*;
import entity.Documentos;
import entity.Usuarios;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class Revision {

    private Date fecha;
    private int pagina;
    private int parrafo;
    private int status;
    private String comentario;
    private Usuarios idRevisor;
    private Documentos idDoc;


    public Revision(Date fecha, int pagina, int parrafo, int status, String comentario, Usuarios idRevisor, Documentos idDoc) {
       this.fecha = fecha;
        this.pagina = pagina;
        this.parrafo = parrafo;
        this.status = status;
        this.comentario = comentario;
        this.idRevisor = idRevisor;
        this.idDoc = idDoc;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public int getParrafo() {
        return parrafo;
    }

    public void setParrafo(int parrafo) {
        this.parrafo = parrafo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Usuarios getIdRevisor() {
        return idRevisor;
    }

    public void setIdRevisor(Usuarios idRevisor) {
        this.idRevisor = idRevisor;
    }

    public Documentos getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Documentos idDoc) {
        this.idDoc = idDoc;
    }
    
    

}
