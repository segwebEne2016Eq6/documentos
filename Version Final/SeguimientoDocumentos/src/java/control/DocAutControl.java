/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import entity.Documentos;
import entity.Usuarios;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import sesion.DocRevFacade;

/**
 *
 * @author Richie1
 */
@ManagedBean
@RequestScoped
public class DocAutControl {
    
    private List<Documentos> listaDocRev2;
    private Usuarios idRev2;

    /**
     * Creates a new instance of DocRev1Control
     */
    public DocAutControl() {
    }
    
    @Inject
    private DocRevFacade docFac;
    
    @PostConstruct
    public void listaDocsRev2(){
        listaDocRev2 = docFac.findDocRevisorDos(2);
    }
    
    public Collection<DocumentoRevisor> listaAut(){
        Collection<DocumentoRevisor> documentos;
        documentos = armaListaAut();
        return documentos;
    }
    public Collection <DocumentoRevisor> armaListaAut(){
          Usuarios idUsuario, idRevi1, idRev2, idAutorizador;
        int status, idDocumento;
        String nombre;
        byte[] file;
        Date fecha;
        

        Collection<DocumentoRevisor> listFile = new ArrayList();
        int i = 0;
        for (Documentos doc : listaDocRev2) {
            i++;
            idDocumento = doc.getIdDocumento();
            idUsuario = doc.getIdUsuario();
            idRev2 = doc.getIdRev2();
            idRevi1 = doc.getIdRev1();
            idAutorizador = doc.getIdAutorizador();
            status = doc.getStatus();
            nombre = doc.getNombre();
            file = doc.getDocumento();
            fecha = doc.getFechaCreacion();

            DocumentoRevisor d = new DocumentoRevisor(idDocumento,file, nombre, status, idUsuario, idRevi1, idRev2, idAutorizador, fecha);
            listFile.add(d);
        }

        return listFile;
    }
    
    
}
