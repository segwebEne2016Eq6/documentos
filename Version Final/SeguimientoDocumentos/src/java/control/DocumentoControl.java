/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import entity.Documentos;
import entity.Usuarios;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import sesion.DocumentoFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class DocumentoControl {

    private Documentos documentoEntidad;
    private List<Documentos> listaDocs;

    public DocumentoControl() {
    }
    
    @EJB
    private DocumentoFacade df;

    @Inject
    private DocumentoFacade docFacade;

    /**
     * Creates a new instance of DocumentoControl
     */
    @PostConstruct
    public void listaDocs() {
        listaDocs = docFacade.findAllbyOrder();

    }

    public Usuarios buscaRevisor1(int id) {
        Usuarios revisor1 = null;
        if (id != 0) {
            revisor1 = docFacade.findByIdRevisor1(id);
            if (revisor1 != null) {
                return revisor1;
            }
        }
        return revisor1;
    }

    public boolean agregaDocumento(Documento documento) {
        boolean error = true;
        documentoEntidad = creaDoc(documento);
        docFacade.save(documentoEntidad);

        return error;
    }

    public Documentos creaDoc(Documento documento) {
      
        Documentos doc = new Documentos();
        Revisores r = new Revisores();
        Usuarios creador = new Usuarios(1);
        Usuarios revuno = new Usuarios(2);
        Usuarios revdos = new Usuarios(3);
        Usuarios autorizador = new Usuarios(4);

        // Usuarios revuno = buscaRevisor1(r.getId());
        //Usuarios revdos = buscaRevisor1(r.getId());
        //Usuarios autorizador = buscaRevisor1(r.getId());
     
        doc.setFechaCreacion(documento.getFecha());
        doc.setDocumento(documento.getFile());
        doc.setNombre(documento.getNombre());
        doc.setIdUsuario(creador);
        doc.setIdRev1(revuno);
        doc.setIdRev2(revdos);
        doc.setIdAutorizador(autorizador);
        return doc;
    }

    public Collection<Documento> listaDocus() {
        Collection<Documento> documentos;
        documentos = armaLista();
        return documentos;
    }

    public Collection<Documento> armaLista() {
        Usuarios idUsuario, idRev1, idRev2, idAutorizador;
        int status, idDoc;
        String nombre;
        byte[] file;
        Date fecha;

        Collection<Documento> listFile = new ArrayList();
        int i = 0;
        for (Documentos doc : listaDocs) {
            i++;
            idUsuario = doc.getIdUsuario();
            idRev2 = doc.getIdRev2();
            idRev1 = doc.getIdRev1();
            idAutorizador = doc.getIdAutorizador();
            status = doc.getStatus();
            nombre = doc.getNombre();
            file = doc.getDocumento();
            fecha = doc.getFechaCreacion();
            idDoc = doc.getIdDocumento();

            Documento d = new Documento(idDoc,file, nombre, status, idUsuario, idRev1, idRev2, idAutorizador, fecha);
            listFile.add(d);
        }

        return listFile;
    }
}
