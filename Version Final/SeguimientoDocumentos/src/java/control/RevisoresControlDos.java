/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import entity.Usuarios;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import sesion.DocumentoFacade;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class RevisoresControlDos {

   private Usuarios usuarioEntidad;
   private List<Usuarios> usuarioEntity;
   
    public RevisoresControlDos() {
    }
    
    @Inject
    private DocumentoFacade documentoFacade;
    
    @PostConstruct
    public void muestraRevisores(){
        usuarioEntity = documentoFacade.findRevisorDos(3);
    }
    
      public Collection<Revisores> listaRevisores(){
        Collection<Revisores> rev;
        rev = armaLista();
        return rev;
    }
    
    private Collection<Revisores> armaLista(){
        int id;
        String nombre;
        
        Collection<Revisores> revisores = new ArrayList();
        int i = 0;
        for(Usuarios revisor: usuarioEntity){
            i++;
            
            id = revisor.getIdUsuario();
            nombre = revisor.getNombre();
            
            Revisores r = new Revisores(id, nombre);
            revisores.add(r);
        }
        
        return revisores;
    }
    
  
    
}
