/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author IthzaVG
 */
@ManagedBean
@RequestScoped
public class Revisores {

  private int id;
  private String nombre;


    public Revisores(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
       
    }
  
    public Revisores(){}
    
  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
