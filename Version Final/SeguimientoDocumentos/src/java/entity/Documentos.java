/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author IthzaVG
 */
@Entity
@Table(name = "documentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documentos.findAll", query = "SELECT d FROM Documentos d"),
    @NamedQuery(name = "Documentos.findByIdDocumento", query = "SELECT d FROM Documentos d WHERE d.idDocumento = :idDocumento"),
    @NamedQuery(name = "Documentos.findByNombre", query = "SELECT d FROM Documentos d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "Documentos.findByFechaCreacion", query = "SELECT d FROM Documentos d WHERE d.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Documentos.findByStatus", query = "SELECT d FROM Documentos d WHERE d.status = :status"),
    @NamedQuery(name = "Documentos.findByFechaAutorizacion", query = "SELECT d FROM Documentos d WHERE d.fechaAutorizacion = :fechaAutorizacion")})
public class Documentos implements Serializable {

    @Basic(optional = false)
    
    @Lob
    @Column(name = "documento")
    private byte[] documento;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_documento")
    private Integer idDocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Column(name = "fecha_autorizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAutorizacion;
    @JoinColumn(name = "id_autorizador", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idAutorizador;
    @JoinColumn(name = "id_rev1", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idRev1;
    @JoinColumn(name = "id_rev2", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idRev2;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDocumento")
    private List<Revisiones> revisionesList;

    public Documentos() {
    }

    public Documentos(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Documentos(Integer idDocumento, String nombre, byte[] documento, int status) {
        this.idDocumento = idDocumento;
        this.nombre = nombre;
        this.documento = documento;
        this.status = status;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public Usuarios getIdAutorizador() {
        return idAutorizador;
    }

    public void setIdAutorizador(Usuarios idAutorizador) {
        this.idAutorizador = idAutorizador;
    }

    public Usuarios getIdRev1() {
        return idRev1;
    }

    public void setIdRev1(Usuarios idRev1) {
        this.idRev1 = idRev1;
    }

    public Usuarios getIdRev2() {
        return idRev2;
    }

    public void setIdRev2(Usuarios idRev2) {
        this.idRev2 = idRev2;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @XmlTransient
    public List<Revisiones> getRevisionesList() {
        return revisionesList;
    }

    public void setRevisionesList(List<Revisiones> revisionesList) {
        this.revisionesList = revisionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumento != null ? idDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documentos)) {
            return false;
        }
        Documentos other = (Documentos) object;
        if ((this.idDocumento == null && other.idDocumento != null) || (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "entity.Documentos[ idDocumento=" + idDocumento + " ]";
        return nombre;
    }

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
    
}
